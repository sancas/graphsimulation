﻿namespace GraphSimulation
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.ribbon1 = new System.Windows.Forms.Ribbon();
            this.ribbonOrbMenuItem1 = new System.Windows.Forms.RibbonOrbMenuItem();
            this.ribbonOrbOptionButton1 = new System.Windows.Forms.RibbonOrbOptionButton();
            this.ribbonOrbRecentItem1 = new System.Windows.Forms.RibbonOrbRecentItem();
            this.ribbonButton2 = new System.Windows.Forms.RibbonButton();
            this.ribbonTab1 = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel1 = new System.Windows.Forms.RibbonPanel();
            this.btnNewFile = new System.Windows.Forms.RibbonButton();
            this.btnSaveFile = new System.Windows.Forms.RibbonButton();
            this.btnPrint = new System.Windows.Forms.RibbonButton();
            this.ribbonPanel3 = new System.Windows.Forms.RibbonPanel();
            this.btnZoomIn = new System.Windows.Forms.RibbonButton();
            this.btnZoomOut = new System.Windows.Forms.RibbonButton();
            this.btnZoomAdjust = new System.Windows.Forms.RibbonButton();
            this.ribbonTab2 = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel2 = new System.Windows.Forms.RibbonPanel();
            this.btnNewNode = new System.Windows.Forms.RibbonButton();
            this.btnDelNode = new System.Windows.Forms.RibbonButton();
            this.btnMoveNode = new System.Windows.Forms.RibbonButton();
            this.ribbonPanel4 = new System.Windows.Forms.RibbonPanel();
            this.ribbonButton1 = new System.Windows.Forms.RibbonButton();
            this.pbCanvas = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.CMSCrearVertice = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.nuevoVerticeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnRecorridoAnchura = new System.Windows.Forms.RibbonButton();
            this.ribbonButton4 = new System.Windows.Forms.RibbonButton();
            this.btnRecorridoProfundidad = new System.Windows.Forms.RibbonButton();
            ((System.ComponentModel.ISupportInitialize)(this.pbCanvas)).BeginInit();
            this.panel1.SuspendLayout();
            this.CMSCrearVertice.SuspendLayout();
            this.SuspendLayout();
            // 
            // ribbon1
            // 
            this.ribbon1.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.ribbon1.Location = new System.Drawing.Point(0, 0);
            this.ribbon1.Name = "ribbon1";
            // 
            // 
            // 
            this.ribbon1.OrbDropDown.BorderRoundness = 8;
            this.ribbon1.OrbDropDown.Location = new System.Drawing.Point(0, 0);
            this.ribbon1.OrbDropDown.MenuItems.Add(this.ribbonOrbMenuItem1);
            this.ribbon1.OrbDropDown.Name = "";
            this.ribbon1.OrbDropDown.OptionItems.Add(this.ribbonOrbOptionButton1);
            this.ribbon1.OrbDropDown.RecentItems.Add(this.ribbonOrbRecentItem1);
            this.ribbon1.OrbDropDown.RecentItemsCaption = null;
            this.ribbon1.OrbDropDown.Size = new System.Drawing.Size(527, 116);
            this.ribbon1.OrbDropDown.TabIndex = 0;
            this.ribbon1.OrbImage = null;
            this.ribbon1.OrbText = null;
            // 
            // 
            // 
            this.ribbon1.QuickAcessToolbar.Image = null;
            this.ribbon1.QuickAcessToolbar.Items.Add(this.ribbonButton2);
            this.ribbon1.QuickAcessToolbar.Tag = null;
            this.ribbon1.QuickAcessToolbar.Value = null;
            this.ribbon1.Size = new System.Drawing.Size(727, 170);
            this.ribbon1.TabIndex = 0;
            this.ribbon1.Tabs.Add(this.ribbonTab1);
            this.ribbon1.Tabs.Add(this.ribbonTab2);
            this.ribbon1.TabsMargin = new System.Windows.Forms.Padding(12, 26, 20, 0);
            this.ribbon1.TabSpacing = 6;
            // 
            // ribbonOrbMenuItem1
            // 
            this.ribbonOrbMenuItem1.AltKey = null;
            this.ribbonOrbMenuItem1.CheckedGroup = null;
            this.ribbonOrbMenuItem1.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.ribbonOrbMenuItem1.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.ribbonOrbMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("ribbonOrbMenuItem1.Image")));
            this.ribbonOrbMenuItem1.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonOrbMenuItem1.SmallImage")));
            this.ribbonOrbMenuItem1.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.ribbonOrbMenuItem1.Tag = null;
            this.ribbonOrbMenuItem1.Text = "ribbonOrbMenuItem1";
            this.ribbonOrbMenuItem1.ToolTip = null;
            this.ribbonOrbMenuItem1.ToolTipTitle = null;
            this.ribbonOrbMenuItem1.Value = null;
            // 
            // ribbonOrbOptionButton1
            // 
            this.ribbonOrbOptionButton1.AltKey = null;
            this.ribbonOrbOptionButton1.CheckedGroup = null;
            this.ribbonOrbOptionButton1.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.ribbonOrbOptionButton1.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.ribbonOrbOptionButton1.Image = ((System.Drawing.Image)(resources.GetObject("ribbonOrbOptionButton1.Image")));
            this.ribbonOrbOptionButton1.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonOrbOptionButton1.SmallImage")));
            this.ribbonOrbOptionButton1.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.ribbonOrbOptionButton1.Tag = null;
            this.ribbonOrbOptionButton1.Text = "ribbonOrbOptionButton1";
            this.ribbonOrbOptionButton1.ToolTip = null;
            this.ribbonOrbOptionButton1.ToolTipTitle = null;
            this.ribbonOrbOptionButton1.Value = null;
            // 
            // ribbonOrbRecentItem1
            // 
            this.ribbonOrbRecentItem1.AltKey = null;
            this.ribbonOrbRecentItem1.CheckedGroup = null;
            this.ribbonOrbRecentItem1.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.ribbonOrbRecentItem1.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.ribbonOrbRecentItem1.Image = ((System.Drawing.Image)(resources.GetObject("ribbonOrbRecentItem1.Image")));
            this.ribbonOrbRecentItem1.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonOrbRecentItem1.SmallImage")));
            this.ribbonOrbRecentItem1.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.ribbonOrbRecentItem1.Tag = null;
            this.ribbonOrbRecentItem1.Text = "ribbonOrbRecentItem1";
            this.ribbonOrbRecentItem1.ToolTip = null;
            this.ribbonOrbRecentItem1.ToolTipTitle = null;
            this.ribbonOrbRecentItem1.Value = null;
            // 
            // ribbonButton2
            // 
            this.ribbonButton2.AltKey = null;
            this.ribbonButton2.CheckedGroup = null;
            this.ribbonButton2.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.ribbonButton2.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.ribbonButton2.Image = ((System.Drawing.Image)(resources.GetObject("ribbonButton2.Image")));
            this.ribbonButton2.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Compact;
            this.ribbonButton2.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton2.SmallImage")));
            this.ribbonButton2.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.ribbonButton2.Tag = null;
            this.ribbonButton2.Text = "ribbonButton2";
            this.ribbonButton2.ToolTip = null;
            this.ribbonButton2.ToolTipTitle = null;
            this.ribbonButton2.Value = null;
            // 
            // ribbonTab1
            // 
            this.ribbonTab1.Panels.Add(this.ribbonPanel1);
            this.ribbonTab1.Panels.Add(this.ribbonPanel3);
            this.ribbonTab1.Tag = null;
            this.ribbonTab1.Text = "Inicio";
            this.ribbonTab1.ToolTip = null;
            this.ribbonTab1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.None;
            this.ribbonTab1.ToolTipImage = null;
            this.ribbonTab1.ToolTipTitle = null;
            this.ribbonTab1.Value = null;
            // 
            // ribbonPanel1
            // 
            this.ribbonPanel1.Items.Add(this.btnNewFile);
            this.ribbonPanel1.Items.Add(this.btnSaveFile);
            this.ribbonPanel1.Items.Add(this.btnPrint);
            this.ribbonPanel1.Tag = null;
            this.ribbonPanel1.Text = "Archivo";
            // 
            // btnNewFile
            // 
            this.btnNewFile.AltKey = null;
            this.btnNewFile.CheckedGroup = null;
            this.btnNewFile.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.btnNewFile.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.btnNewFile.Image = ((System.Drawing.Image)(resources.GetObject("btnNewFile.Image")));
            this.btnNewFile.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnNewFile.SmallImage")));
            this.btnNewFile.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.btnNewFile.Tag = null;
            this.btnNewFile.Text = "Nuevo";
            this.btnNewFile.ToolTip = null;
            this.btnNewFile.ToolTipTitle = null;
            this.btnNewFile.Value = null;
            this.btnNewFile.Click += new System.EventHandler(this.btnNewFile_Click);
            // 
            // btnSaveFile
            // 
            this.btnSaveFile.AltKey = null;
            this.btnSaveFile.CheckedGroup = null;
            this.btnSaveFile.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.btnSaveFile.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.btnSaveFile.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveFile.Image")));
            this.btnSaveFile.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnSaveFile.SmallImage")));
            this.btnSaveFile.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.btnSaveFile.Tag = null;
            this.btnSaveFile.Text = "Guardar";
            this.btnSaveFile.ToolTip = null;
            this.btnSaveFile.ToolTipTitle = null;
            this.btnSaveFile.Value = null;
            this.btnSaveFile.Click += new System.EventHandler(this.btnSaveFile_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.AltKey = null;
            this.btnPrint.CheckedGroup = null;
            this.btnPrint.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.btnPrint.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.btnPrint.Image = ((System.Drawing.Image)(resources.GetObject("btnPrint.Image")));
            this.btnPrint.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnPrint.SmallImage")));
            this.btnPrint.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.btnPrint.Tag = null;
            this.btnPrint.Text = "Imprimir";
            this.btnPrint.ToolTip = null;
            this.btnPrint.ToolTipTitle = null;
            this.btnPrint.Value = null;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // ribbonPanel3
            // 
            this.ribbonPanel3.Items.Add(this.btnZoomIn);
            this.ribbonPanel3.Items.Add(this.btnZoomOut);
            this.ribbonPanel3.Items.Add(this.btnZoomAdjust);
            this.ribbonPanel3.Tag = null;
            this.ribbonPanel3.Text = "Zoom";
            // 
            // btnZoomIn
            // 
            this.btnZoomIn.AltKey = null;
            this.btnZoomIn.CheckedGroup = null;
            this.btnZoomIn.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.btnZoomIn.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.btnZoomIn.Image = ((System.Drawing.Image)(resources.GetObject("btnZoomIn.Image")));
            this.btnZoomIn.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnZoomIn.SmallImage")));
            this.btnZoomIn.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.btnZoomIn.Tag = null;
            this.btnZoomIn.Text = "Zoom+";
            this.btnZoomIn.ToolTip = null;
            this.btnZoomIn.ToolTipTitle = null;
            this.btnZoomIn.Value = null;
            this.btnZoomIn.Click += new System.EventHandler(this.btnZoomIn_Click);
            // 
            // btnZoomOut
            // 
            this.btnZoomOut.AltKey = null;
            this.btnZoomOut.CheckedGroup = null;
            this.btnZoomOut.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.btnZoomOut.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.btnZoomOut.Image = ((System.Drawing.Image)(resources.GetObject("btnZoomOut.Image")));
            this.btnZoomOut.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnZoomOut.SmallImage")));
            this.btnZoomOut.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.btnZoomOut.Tag = null;
            this.btnZoomOut.Text = "Zoom-";
            this.btnZoomOut.ToolTip = null;
            this.btnZoomOut.ToolTipTitle = null;
            this.btnZoomOut.Value = null;
            this.btnZoomOut.Click += new System.EventHandler(this.btnZoomOut_Click);
            // 
            // btnZoomAdjust
            // 
            this.btnZoomAdjust.AltKey = null;
            this.btnZoomAdjust.CheckedGroup = null;
            this.btnZoomAdjust.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.btnZoomAdjust.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.btnZoomAdjust.Image = ((System.Drawing.Image)(resources.GetObject("btnZoomAdjust.Image")));
            this.btnZoomAdjust.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnZoomAdjust.SmallImage")));
            this.btnZoomAdjust.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.btnZoomAdjust.Tag = null;
            this.btnZoomAdjust.Text = "Ajustar";
            this.btnZoomAdjust.ToolTip = null;
            this.btnZoomAdjust.ToolTipTitle = null;
            this.btnZoomAdjust.Value = null;
            this.btnZoomAdjust.Click += new System.EventHandler(this.btnZoomAdjust_Click);
            // 
            // ribbonTab2
            // 
            this.ribbonTab2.Panels.Add(this.ribbonPanel2);
            this.ribbonTab2.Panels.Add(this.ribbonPanel4);
            this.ribbonTab2.Tag = null;
            this.ribbonTab2.Text = "Nodo";
            this.ribbonTab2.ToolTip = null;
            this.ribbonTab2.ToolTipIcon = System.Windows.Forms.ToolTipIcon.None;
            this.ribbonTab2.ToolTipImage = null;
            this.ribbonTab2.ToolTipTitle = null;
            this.ribbonTab2.Value = null;
            // 
            // ribbonPanel2
            // 
            this.ribbonPanel2.Items.Add(this.btnNewNode);
            this.ribbonPanel2.Items.Add(this.btnDelNode);
            this.ribbonPanel2.Items.Add(this.btnMoveNode);
            this.ribbonPanel2.Tag = null;
            this.ribbonPanel2.Text = "Administrar";
            // 
            // btnNewNode
            // 
            this.btnNewNode.AltKey = null;
            this.btnNewNode.CheckedGroup = null;
            this.btnNewNode.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.btnNewNode.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.btnNewNode.Image = ((System.Drawing.Image)(resources.GetObject("btnNewNode.Image")));
            this.btnNewNode.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnNewNode.SmallImage")));
            this.btnNewNode.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.btnNewNode.Tag = null;
            this.btnNewNode.Text = "Nuevo";
            this.btnNewNode.ToolTip = null;
            this.btnNewNode.ToolTipTitle = null;
            this.btnNewNode.Value = null;
            this.btnNewNode.Click += new System.EventHandler(this.btnNewNode_Click);
            // 
            // btnDelNode
            // 
            this.btnDelNode.AltKey = null;
            this.btnDelNode.CheckedGroup = null;
            this.btnDelNode.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.btnDelNode.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.btnDelNode.Image = ((System.Drawing.Image)(resources.GetObject("btnDelNode.Image")));
            this.btnDelNode.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnDelNode.SmallImage")));
            this.btnDelNode.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.btnDelNode.Tag = null;
            this.btnDelNode.Text = "Eliminar";
            this.btnDelNode.ToolTip = null;
            this.btnDelNode.ToolTipTitle = null;
            this.btnDelNode.Value = null;
            this.btnDelNode.Click += new System.EventHandler(this.btnDelNode_Click);
            // 
            // btnMoveNode
            // 
            this.btnMoveNode.AltKey = null;
            this.btnMoveNode.CheckedGroup = null;
            this.btnMoveNode.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.btnMoveNode.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.btnMoveNode.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveNode.Image")));
            this.btnMoveNode.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnMoveNode.SmallImage")));
            this.btnMoveNode.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.btnMoveNode.Tag = null;
            this.btnMoveNode.Text = "Mover";
            this.btnMoveNode.ToolTip = null;
            this.btnMoveNode.ToolTipTitle = null;
            this.btnMoveNode.Value = null;
            this.btnMoveNode.Click += new System.EventHandler(this.btnMoveNode_Click);
            // 
            // ribbonPanel4
            // 
            this.ribbonPanel4.Items.Add(this.btnRecorridoAnchura);
            this.ribbonPanel4.Items.Add(this.btnRecorridoProfundidad);
            this.ribbonPanel4.Tag = null;
            this.ribbonPanel4.Text = "Recorridos";
            // 
            // ribbonButton1
            // 
            this.ribbonButton1.AltKey = null;
            this.ribbonButton1.CheckedGroup = null;
            this.ribbonButton1.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.ribbonButton1.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.ribbonButton1.Image = ((System.Drawing.Image)(resources.GetObject("ribbonButton1.Image")));
            this.ribbonButton1.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton1.SmallImage")));
            this.ribbonButton1.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.ribbonButton1.Tag = null;
            this.ribbonButton1.Text = "ribbonButton1";
            this.ribbonButton1.ToolTip = null;
            this.ribbonButton1.ToolTipTitle = null;
            this.ribbonButton1.Value = null;
            // 
            // pbCanvas
            // 
            this.pbCanvas.BackColor = System.Drawing.SystemColors.Info;
            this.pbCanvas.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbCanvas.Enabled = false;
            this.pbCanvas.Location = new System.Drawing.Point(0, 0);
            this.pbCanvas.Name = "pbCanvas";
            this.pbCanvas.Size = new System.Drawing.Size(100, 100);
            this.pbCanvas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbCanvas.TabIndex = 1;
            this.pbCanvas.TabStop = false;
            this.pbCanvas.Visible = false;
            this.pbCanvas.Paint += new System.Windows.Forms.PaintEventHandler(this.pbCanvas_Paint);
            this.pbCanvas.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pbCanvas_MouseDown);
            this.pbCanvas.MouseLeave += new System.EventHandler(this.pbCanvas_MouseLeave);
            this.pbCanvas.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pbCanvas_MouseMove);
            this.pbCanvas.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pbCanvas_MouseUp);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.pbCanvas);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 170);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(727, 273);
            this.panel1.TabIndex = 2;
            // 
            // CMSCrearVertice
            // 
            this.CMSCrearVertice.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuevoVerticeToolStripMenuItem});
            this.CMSCrearVertice.Name = "CMSCrearVertice";
            this.CMSCrearVertice.Size = new System.Drawing.Size(148, 26);
            // 
            // nuevoVerticeToolStripMenuItem
            // 
            this.nuevoVerticeToolStripMenuItem.Name = "nuevoVerticeToolStripMenuItem";
            this.nuevoVerticeToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.nuevoVerticeToolStripMenuItem.Text = "Nuevo vertice";
            this.nuevoVerticeToolStripMenuItem.Click += new System.EventHandler(this.nuevoVerticeToolStripMenuItem_Click);
            // 
            // btnRecorridoAnchura
            // 
            this.btnRecorridoAnchura.AltKey = null;
            this.btnRecorridoAnchura.CheckedGroup = null;
            this.btnRecorridoAnchura.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.btnRecorridoAnchura.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.btnRecorridoAnchura.DropDownItems.Add(this.ribbonButton4);
            this.btnRecorridoAnchura.Image = ((System.Drawing.Image)(resources.GetObject("btnRecorridoAnchura.Image")));
            this.btnRecorridoAnchura.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnRecorridoAnchura.SmallImage")));
            this.btnRecorridoAnchura.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.btnRecorridoAnchura.Tag = null;
            this.btnRecorridoAnchura.Text = "Anchura";
            this.btnRecorridoAnchura.ToolTip = null;
            this.btnRecorridoAnchura.ToolTipTitle = null;
            this.btnRecorridoAnchura.Value = null;
            this.btnRecorridoAnchura.Click += new System.EventHandler(this.btnRecorridoAnchura_Click);
            // 
            // ribbonButton4
            // 
            this.ribbonButton4.AltKey = null;
            this.ribbonButton4.CheckedGroup = null;
            this.ribbonButton4.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.ribbonButton4.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.ribbonButton4.Image = ((System.Drawing.Image)(resources.GetObject("ribbonButton4.Image")));
            this.ribbonButton4.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton4.SmallImage")));
            this.ribbonButton4.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.ribbonButton4.Tag = null;
            this.ribbonButton4.Text = "ribbonButton4";
            this.ribbonButton4.ToolTip = null;
            this.ribbonButton4.ToolTipTitle = null;
            this.ribbonButton4.Value = null;
            // 
            // btnRecorridoProfundidad
            // 
            this.btnRecorridoProfundidad.AltKey = null;
            this.btnRecorridoProfundidad.CheckedGroup = null;
            this.btnRecorridoProfundidad.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.btnRecorridoProfundidad.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.btnRecorridoProfundidad.Image = ((System.Drawing.Image)(resources.GetObject("btnRecorridoProfundidad.Image")));
            this.btnRecorridoProfundidad.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnRecorridoProfundidad.SmallImage")));
            this.btnRecorridoProfundidad.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.btnRecorridoProfundidad.Tag = null;
            this.btnRecorridoProfundidad.Text = "Profundidad";
            this.btnRecorridoProfundidad.ToolTip = null;
            this.btnRecorridoProfundidad.ToolTipTitle = null;
            this.btnRecorridoProfundidad.Value = null;
            this.btnRecorridoProfundidad.Click += new System.EventHandler(this.btnRecorridoProfundidad_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(727, 443);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.ribbon1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Simulador de grafos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.pbCanvas)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.CMSCrearVertice.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        private System.Windows.Forms.Ribbon ribbon1;

        #endregion
        private System.Windows.Forms.RibbonTab ribbonTab1;
        private System.Windows.Forms.RibbonPanel ribbonPanel1;
        private System.Windows.Forms.RibbonTab ribbonTab2;
        private System.Windows.Forms.RibbonPanel ribbonPanel2;
        private System.Windows.Forms.RibbonButton btnNewNode;
        private System.Windows.Forms.RibbonButton btnDelNode;
        private System.Windows.Forms.RibbonButton btnMoveNode;
        private System.Windows.Forms.RibbonButton btnNewFile;
        private System.Windows.Forms.RibbonButton btnSaveFile;
        private System.Windows.Forms.RibbonPanel ribbonPanel3;
        private System.Windows.Forms.RibbonButton btnZoomIn;
        private System.Windows.Forms.RibbonButton btnZoomOut;
        private System.Windows.Forms.RibbonButton btnZoomAdjust;
        private System.Windows.Forms.RibbonButton btnPrint;
        private System.Windows.Forms.RibbonOrbMenuItem ribbonOrbMenuItem1;
        private System.Windows.Forms.RibbonOrbOptionButton ribbonOrbOptionButton1;
        private System.Windows.Forms.RibbonOrbRecentItem ribbonOrbRecentItem1;
        private System.Windows.Forms.RibbonButton ribbonButton1;
        private System.Windows.Forms.RibbonButton ribbonButton2;
        private System.Windows.Forms.PictureBox pbCanvas;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ContextMenuStrip CMSCrearVertice;
        private System.Windows.Forms.ToolStripMenuItem nuevoVerticeToolStripMenuItem;
        private System.Windows.Forms.RibbonPanel ribbonPanel4;
        private System.Windows.Forms.RibbonButton btnRecorridoAnchura;
        private System.Windows.Forms.RibbonButton ribbonButton4;
        private System.Windows.Forms.RibbonButton btnRecorridoProfundidad;
    }
}

